/**
 * Created by asanchez75 on 24/03/2017.
 */

angular.module('app.services', [])

    .service('EmbalsesDataService', function($http) {
	var dataReportTable = [];
	var nr = [];
	var chu = [];
	var cha = [];
	var chartEmbalses = [];

	$http.get('js/embalses.json').then(function(response){
		angular.forEach(response.data, function(value, key) {
			dataReportTable.push(value);
			nr.push(value.Nombre_Reservorio);
			chu.push(parseInt(value.Capacid_Hidraul_Util));
			cha.push(parseInt(value.Capacid_Hidraul_Alm));
		});

		chartEmbalses.embRepoTable = dataReportTable;
		chartEmbalses.nombres = nr;
		chartEmbalses.cap_hi_u = chu;
		chartEmbalses.cap_hi_a = cha;
	});

	return chartEmbalses;

    })

    .service('PouchService', ['$rootScope','$window','$q' ,'$injector', 'pouchDB', function($rootScope, $window, $q, $injector ,pouchDB){

        service = this;

        var db = pouchDB('embalses');

        db.changes({
            since: 'now',
            live: true
        }).on('change', function(){
            $rootScope.$broadcast('db:changes');
        });

        service.db = db;

        service.rows = function(view) {
            return db.query(view, {include_docs: true, attachments: true, descending: true}).then(
                function(data){
                    return data
                });
        }

        // it is a generic view to run any type of query
        service.query = function(viewname, options) {
            return db.query(viewname, options).then(
                function(data){
                    return data
                });
        }

        service.$put = function(item) {
            db.put(item);
        }

        service.$remove = function(item) {
            db.remove(item);
        }

        service.$add = function(item) {
            db.post(angular.copy(item)).then(
                function(res) {
                    item._rev = res.rev;
                    item._id = res.id;
                }
            );
        };

        service.$update = function(item) {
            var copy = {};
            angular.forEach(item, function(value, key) {
                if (key.indexOf('$') !== 0) {
                    copy[key] = value;
                }
            });
            db.get(item._id).then(
                function (res) {
                    db.put(copy, res._rev);
                }
            );
        };
        // create a initial embedded view for favoritos
        service.initFavoritos = function() {
            var ddoc = {
                _id: "_design/embalses",
                views: {
                    favoritos: {
                        map: "function(doc) { if (doc.type == 'favoritos') { emit(doc._id, doc); } }"
                    }
                },
                language: "javascript"
            }
            db.put(ddoc);
        }


    }])

    .service('GeneratorIDsService', function() {
        service = this;
        service.helpers = {
            generate_id: function(label) {
                return label.toLowerCase().replace(' ', '_').replace(' ', '_').normalize('NFD').replace(/[\u0300-\u036f]/g,"");
            }
        };

    })


;
