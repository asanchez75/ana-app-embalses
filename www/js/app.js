// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js


angular.module('starter', ['ionic', 'app.controllers', 'app.services', 'highcharts-ng', 'pouchdb' , 'ui.router'])

.run(function($ionicPlatform, PouchService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    window.addEventListener("orientationchange", function() {
      if(window.innerWidth > 600){
        angular.element(document.querySelectorAll('.repo-hide-info')).css('display', 'table-cell');
        angular.element(document.querySelectorAll('.repo-head-title .sdt')).css('display', 'none');
        angular.element(document.querySelectorAll('.repo-head-title .hdt')).css('display', 'none');
      }else{
        angular.element(document.querySelectorAll('.repo-hide-info')).css('display', 'none');
        angular.element(document.querySelectorAll('.repo-head-title .sdt')).css('display', 'inline-block');
      }
    }, false);

    // initializing pouchdb view
    PouchService.initFavoritos();

  });
})
/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
*/
.directive('hrefInappbrowser', function() {
  return {
    restrict: 'A',
    replace: false,
    transclude: false,
    link: function(scope, element, attrs) {
      var href = attrs['hrefInappbrowser'];

      attrs.$observe('hrefInappbrowser', function(val){
        href = val;
      });

      element.bind('click', function (event) {

        window.open(href, '_system', 'location=yes');

        event.preventDefault();
        event.stopPropagation();

      });
    }
  };
})
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $stateProvider
  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'LoginCtrl'
  })

  .state('app.embalses', {
    cache: true,
    url: "/embalses",
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalses.html",
        controller: 'EmbalsesCtrl'
      }
    }
  })

  .state('app.embalse', {
    cache: false,
    url: "/embalse",
    params: { itemEmbMap: null },
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseSeleInfo.html",
        controller: 'EmbalseCtrl'
      }
    }
  })

  /******************Maps Shared States**********************/
  .state('app.embalseMapPhotos', {
    cache: false,
    url: "/embalseMapPhotos",
    params: { item: null, photos: null },
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseSeleFotos.html",
        controller: 'EmbalseMapPhotosCtrl'
      }
    }
  })

  .state('app.embalseMapSeleGene', {
    cache: false,
    url: "/embalseMapSeleGene",
    params: { item: null, photos: null },
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseSeleInfoGene.html",
        controller: 'EmbalseMapSeleGeneCtrl'
      }
    }
  })

  .state('app.embalseMapSeleRegisHis', {
    cache: false,
    url: "/embalseMapSeleRegisHis",
    params: { item: null},
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseSeleRegisHis.html",
        controller: 'EmbalseMapSeleRegisHisCtrl'
      }
    }
  })

  .state('app.embalseMapSeleRegisHisChart', {
    cache: false,
    url: "/embalseMapSeleRegisHisChart",
    params: { item: null},
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseSeleRegisHisChart.html",
        controller: 'EmbalseMapSeleRegisHisChartCtrl'
      }
    }
  })

  .state('app.embalseMapGlosario', {
    cache: false,
    url: "/embalseMapGlosario",
    params: { glosario: null },
    views: {
      'tab-embalses': {
        templateUrl: "templates/embalseGlosario.html",
        controller: 'EmbalseMapGlosarioCtrl'
      }
    }
  })
  /*********************************************************/

  .state('app.reportes', {
    cache: true,
    url: "/reportes",
    views: {
      'tab-reportes': {
        templateUrl: "templates/reportes.html",
        controller: 'ReportesCtrl'
      }
    }
  })

  .state('app.reportesTABLE', {
    cache: true,
    url: "/reportesTable",
    params: { dataTableReport: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/reportesTABLE.html",
        controller: 'ReportesTableCtrl'
      }
    }
  })

  .state('app.reportesDepaSel', {
    cache: false,
    url: "/reportesDepaSel",
    params: { embalseRepoDtos: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/reportesDepaSel.html",
        controller: 'ReportesDepaSelCtrl'
      }
    }
  })

  .state('app.reportesAAASel', {
    cache: false,
    url: "/reportesAAASel",
    params: { embalseRepoAAA: null, embalseRepoAAAEmb: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/reportesAAASel.html",
        controller: 'ReportesAAASelCtrl'
      }
    }
  })

  .state('app.reportesAAASelResult', {
    cache: false,
    url: "/reportesAAASelResult",
    params: { item: null},
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseSeleInfo.html",
        controller: 'ReportesAAASelResultCtrl'
      }
    }
  })

  /******************Reports Shared States**********************/
  .state('app.reportesAAAPhotos', {
    cache: false,
    url: "/reportesAAAPhotos",
    params: { item: null, photos: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseSeleFotos.html",
        controller: 'ReportesAAAPhotosCtrl'
      }
    }
  })

  .state('app.reportesAAASeleGene', {
    cache: false,
    url: "/reportesAAASeleGene",
    params: { item: null, photos: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseSeleInfoGene.html",
        controller: 'ReportesAAASeleGeneCtrl'
      }
    }
  })

  .state('app.reportesAAASeleRegisHis', {
    cache: false,
    url: "/reportesAAASeleRegisHis",
    params: { item: null},
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseSeleRegisHis.html",
        controller: 'ReportesAAASeleRegisHisCtrl'
      }
    }
  })

  .state('app.reportesAAASeleRegisHisChart', {
    cache: false,
    url: "/reportesAAASeleRegisHisChart",
    params: { item: null},
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseSeleRegisHisChart.html",
        controller: 'ReportesAAASeleRegisHisChartCtrl'
      }
    }
  })

  .state('app.reportesAAAGlosario', {
    cache: false,
    url: "/reportesAAAGlosario",
    params: { glosario: null },
    views: {
      'tab-reportes': {
        templateUrl: "templates/embalseGlosario.html",
        controller: 'ReportesAAAGlosarioCtrl'
      }
    }
  })
  /****************************************/

  .state('app.misEmbalses', {
    cache: false,
    url: "/misEmbalses",
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/misEmbalses.html",
        controller: 'MisEmbalsesCtrl'
      }
    },
    resolve: {
      Favoritos : function(PouchService) {
        return PouchService.rows('embalses/favoritos');
      }
    }
  })

  //@todo -> to remove?
  .state('app.embalsesSele', {
    cache: false,
    url: "/embalsesSele",
    params: { esl: null },
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalsesSele.html",
        controller: 'EmbalsesSeleCtrl'
      }
    }
  })

  .state('app.embalseSeleInfo', {
    cache: false,
    url: "/embalseSeleInfo",
    params: { item: null },
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseSeleInfo.html",
        controller: 'EmbalseSeleInfoCtrl'
      }
    }
  })

  .state('app.embalseSeleInfoGene', {
    cache: false,
    url: "/embalseSeleInfoGene",
    params: { item: null, photos: null },
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseSeleInfoGene.html",
        controller: 'EmbalseSeleInfoGeneCtrl'
      }
    }
  })

  .state('app.embalseSeleRegisHis', {
    cache: false,
    url: "/embalseSeleRegisHis",
    params: { item: null},
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseSeleRegisHis.html",
        controller: 'EmbalseSeleRegisHisCtrl'
      }
    }
  })

  .state('app.embalseSeleRegisHisChart', {
    cache: false,
    url: "/embalseSeleRegisHisChart",
    params: { item: null},
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseSeleRegisHisChart.html",
        controller: 'EmbalseSeleRegisHisChartCtrl'
      }
    }
  })

  .state('app.embalseSeleFotos', {
    cache: false,
    url: "/embalseSeleFotos",
    params: { item: null, photos: null },
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseSeleFotos.html",
        controller: 'EmbalseSeleFotosCtrl'
      }
    }
  })

  .state('app.embalseGlosario', {
    cache: false,
    url: "/embalseGlosario",
    params: { glosario: null },
    views: {
      'tab-misEmbalses': {
        templateUrl: "templates/embalseGlosario.html",
        controller: 'EmbalseGlosarioCtrl'
      }
    }
  })

  .state('app.acerca', {
    url: "/acerca",
    views: {
      'tab-acerca': {
        templateUrl: "templates/acerca.html",
        controller: 'AcercaCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('app/embalses');
});
