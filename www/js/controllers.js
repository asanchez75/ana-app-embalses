angular.module('app.controllers', [])

.controller('LoginCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('ReportesCtrl', function($scope, $http, EmbalsesDataService) {
  $scope.dataTableReport = EmbalsesDataService.embRepoTable;
  $scope.dataMarkers = new Array();
  $http.get('js/markers_data.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      $scope.dataMarkers.push(value);
    });
  });

  $scope.chartConfig = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volumen de Agua Almacenado en los Reservorios Principales del País'
    },
    subtitle: {
        text: '(Porcentaje de Almacenamiento)'
    },
    xAxis: {
        categories: EmbalsesDataService.nombres
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }/*,
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }*/
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            /*dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
            }*/
        }
    },
    series: [{
        name: 'Almacenada',
        data: EmbalsesDataService.cap_hi_u
    }, {
        name: 'No Utilizada',
        data: EmbalsesDataService.cap_hi_a
    }]
  }
})

.controller('ReportesTableCtrl', function($scope, $http, $state) {
  $scope.dataTableReport = $state.params.dataTableReport;

  $scope.showDataHidden = function(event){
    angular.element(document.querySelectorAll('.repo-hide-info')).css('display', 'none');
    angular.element(document.querySelectorAll('.repo-head-title .hdt')).css('display', 'none');
    angular.element(document.querySelectorAll('.repo-head-title .sdt')).css('display', 'inline-block');
    angular.element(event.currentTarget.querySelectorAll('.repo-hide-info')).css('display', 'block');
    angular.element(event.currentTarget.querySelectorAll('.repo-hide-info')).css('display', 'block');
    angular.element(event.currentTarget.querySelectorAll('.repo-head-title .sdt')).css('display', 'none');
    angular.element(event.currentTarget.querySelectorAll('.repo-head-title .hdt')).css('display', 'inline-block');
  };
})

.controller('ReportesDepaSelCtrl', function($scope, $http, $state) {
  $scope.dataParam = $state.params.embalseRepoDtos;

  $scope.result = null;
  $scope.dptos = new Array();
  $scope.embs;

  //Crea un array con los Departamentos disponibles
  $scope.dataParam.forEach(function (obj) {
    if (!this[obj.Depart_Imfluenc]) {
        this[obj.Depart_Imfluenc] = {dpto: obj.Depart_Imfluenc};
        $scope.dptos.push(this[obj.Depart_Imfluenc]);
    }
  }, Object.create(null));

  $scope.onChangeDPTO = function(){
    $scope.embs = new Array();
    angular.forEach($scope.dataParam, function(value, key) {
      if(value.Depart_Imfluenc == $scope.dptos.dpto){
        $scope.embs.push({emb: value.Nombre_Reservorio});
      }
    });
  }

  $scope.onChangeEMB = function(){
    angular.forEach($scope.dataParam, function(value, key) {
      if(value.Nombre_Reservorio == $scope.embs.emb){
        $scope.result = value;
      }
    });
  }

  $scope.onClickRepoDPTOEmb = function(){
    if($scope.result != null){
      $state.go('app.reportesAAASelResult', {item: [$scope.result.Nombre_Reservorio, $scope.result]});
    }
  }
})

.controller('ReportesAAASelCtrl', function($scope, $http, $state) {
  $scope.result = null;
  $scope.aaas = new Array();
  $scope._aaa;

  $scope.embs;
  $scope._embs;

  $scope.alas;
  $scope._ala;
  $scope._alas;

  $scope.dataAAA = $state.params.embalseRepoAAA;
  $scope.dataEmbaaa = $state.params.embalseRepoAAAEmb;

  $scope.dataAAA.forEach(function (obj) {
    if (!this[obj.aaa]) {
        this[obj.aaa] = {aaa: obj.aaa};
        $scope.aaas.push(this[obj.aaa]);
    }
  }, Object.create(null));

  $scope.onChangeAAA = function(){
    $scope._aaa = $scope.aaas.aaa;
    $scope.alas = new Array();
    $scope._alas = new Array();

    $scope._embs = new Array();
    $scope.embs = new Array();

    angular.forEach($scope.dataAAA, function(value, key) {
      if(value.aaa == $scope.aaas.aaa){
        $scope._alas.push({ala: value.ala});
      }
    });

    if($scope._alas.length > 0){
      $scope._alas.forEach(function (obj) {
        if (!this[obj.ala]) {
            this[obj.ala] = {ala: obj.ala};
            $scope.alas.push(this[obj.ala]);
        }
      }, Object.create(null));
    }
  }

  $scope.onChangeALA = function(){
    $scope._embs = new Array();
    $scope.embs = new Array();
    $scope._ala = $scope.alas.ala;
    if($scope._alas.length > 0){
      angular.forEach($scope.dataAAA, function(value, key) {
        if(value.aaa == $scope._aaa && value.ala == $scope._ala){
          $scope._embs.push(value);
        }
      });
    }

    if($scope._embs.length > 0){
      $scope._embs.forEach(function (obj) {
        if (!this[obj.embalse]) {
            this[obj.embalse] = {embalse: obj.embalse};
            $scope.embs.push(this[obj.embalse]);
        }
      }, Object.create(null));
    }
  }

  $scope.onChangeEMB = function(){
    angular.forEach($scope.dataEmbaaa, function(value, key) {
      if(value.Nombre_Reservorio == $scope.embs.embalse){
        $scope.result = value;
      }
    });
  }

  $scope.onClickRepoAAAEmb = function(){
    if($scope.result != null){
      $state.go('app.reportesAAASelResult', {item: [$scope.result.Nombre_Reservorio, $scope.result]});
    }
  }
})

.controller('ReportesAAASelResultCtrl', function($scope, $http, $state) {
  $scope.item = $state.params.item;

  /*Data Chart*/
  $scope.chartConfig = {
    chart: {
        type: 'bar',
        height: 150,
    },
    title: {
        text: ''
    },
    subtitle: {
        text: '(Volumen de Agua)'
    },
    xAxis: {
        categories: ['%']
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: ''
        }/*,
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }*/
    },
    legend: {
        reversed: true
    },
    tooltip: {
        pointFormat: 'Porcentaje: <b>{point.y:.1f} %</b>'
    },
    plotOptions: {
        bar: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
            }
        }
    },
    series: [{
        name: 'No Utilizada',
        data: [(100 - parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc))],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    },{
        name: 'Almacenada',
        data: [parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc)],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    }]
  }
  /*Data Chart *******************************************************/

  $scope.glosario = new Array();
  $scope.photos = new Array();
  $http.get('js/embalses_photos.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      if($scope.item[1].Nombre_Reservorio == value.embalse){
        angular.forEach(value.photos, function(foto, k) {
          $http.get('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto).success(function(data){
            $scope.photos.push('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto);
          }).error(function(error){
          });
        });
      }
    });
  });

  $http.get('js/glosario.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      $scope.glosario.push(value);
    });
  });

  $scope.seePhotos = function(){
    $state.go('app.reportesAAAPhotos', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelGene = function(){
    $state.go('app.reportesAAASeleGene', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.reportesAAASeleRegisHis', {item: $scope.item});
  }

  $scope.showGlosario = function(){
    $state.go('app.reportesAAAGlosario', {glosario: $scope.glosario});
  }
})

.controller('MisEmbalsesCtrl', ['$scope', '$http', '$state', 'PouchService', 'Favoritos', 'GeneratorIDsService', function($scope, $http, $state, PouchService, Favoritos, GeneratorIDsService) {
  //console.log(Favoritos);
  if (Favoritos.rows.length > 0) {

      $scope.favoritos = true;

      var elements = [];

      angular.forEach(Favoritos.rows, function(value, key) {
          value.doc.checked = true;
          elements.push(value.doc);
      });

      $scope.misEmbalses = [];


      elements.forEach(function (a) {
          if (!this[a.Zona]) {
              this[a.Zona] = {zona: a.Zona};
              $scope.misEmbalses.push(this[a.Zona]);
          }
      }, Object.create(null));

      angular.forEach($scope.misEmbalses, function(value, key) {
          var res = [];
          angular.forEach(elements, function(v, k) {
              if(value.zona == v.Zona){
                  res.push(v);
              }
          });
          value.reservorios = res;
      });
      //console.log($scope.misEmbalses);

  }
  else {

      $scope.favoritos = false;

      $http.get('js/embalses.json').then(function(response){
          $scope.misEmbalses = [];
          response.data.forEach(function (a) {
              if (!this[a.Zona]) {
                  this[a.Zona] = {zona: a.Zona};
                  $scope.misEmbalses.push(this[a.Zona]);
              }
          }, Object.create(null));
          // console.log($scope.misEmbalses);

          angular.forEach($scope.misEmbalses, function(value, key) {
              var res = [];
              angular.forEach(response.data, function(v, k) {
                  if(value.zona == v.Zona){
                      res.push(v);
                  }
              });
              value.reservorios = res;
          });
          // console.log($scope.misEmbalses);
      });


      $scope.emb_selected = [];

      //Add and Remove selected checkbox from list
      $scope.getIdCheckBoxList = function(idParent, idItem){
          if(this.checked){
              $scope.emb_selected.push([idParent, idItem]);
          }else{
              angular.forEach($scope.emb_selected, function(value, key) {
                  if(value[0] == idParent && value[1] == idItem){
                      $scope.emb_selected.splice(key, 1);
                  }
              });
          }
          // console.log($scope.emb_selected);
          $scope.sel_emb = new Array();
          angular.forEach($scope.emb_selected, function(value, key) {
              $scope.sel_emb.push([value[0],$scope.misEmbalses[value[0]].reservorios[value[1]]]);
          });
          // console.log($scope.sel_emb);
      };
  }

  $scope.reset = function() {
      if (Favoritos.rows.length > 0) {
          angular.forEach(Favoritos.rows, function(value, key) {
              PouchService.$remove(value.doc);
              $state.go('app.misEmbalses', {}, {reload: true});
          });
      }
  }

  $scope.saveFavoritos = function(items) {
      angular.forEach(items, function(value, key) {
          var item = value[1];
          item._id = GeneratorIDsService.helpers.generate_id(item.Zona + '-' + item.Nombre_Reservorio);
          item.type = 'favoritos';

          PouchService.$put(item);
          $state.go('app.misEmbalses', {}, {reload: true});
          //$rootScope.$on('db:changes', function(){
          //console.log('values saved');
          //console.log(item);
          //});

      });
  }

}])

//@todo -> to remove?
.controller('EmbalsesSeleCtrl', ['$scope', '$state', 'PouchService', '$rootScope', 'GeneratorIDsService', function($scope, $state, PouchService, $rootScope, GeneratorIDsService) {
  $scope.esl = $state.params.esl;
}])

.controller('EmbalseSeleInfoCtrl', function($scope, $state, $http) {

  $scope.item = $state.params.item;

  // This a workaround if data comes from favoritos.
  // Usually this is an array but it is a object when comes from favoritos section
  // then, the array is rebuilt by hand
  if (typeof $scope.item === 'object') {
      $scope.item[0] = 0;
      $scope.item[1] = $scope.item;
  }

  /*Data Chart*/
  $scope.chartConfig = {
    chart: {
        type: 'bar',
        height: 150,
    },
    title: {
        text: ''
    },
    subtitle: {
        text: '(Volumen de Agua)'
    },
    xAxis: {
        categories: ['%']
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: ''
        }/*,
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }*/
    },
    legend: {
        reversed: true
    },
    tooltip: {
        pointFormat: 'Porcentaje: <b>{point.y:.1f} %</b>'
    },
    plotOptions: {
        bar: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
            }
        }
    },
    series: [{
        name: 'No Utilizada',
        data: [(100 - parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc))],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    },{
        name: 'Almacenada',
        data: [parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc)],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    }]
  }
  /*Data Chart *******************************************************/

  $scope.glosario = new Array();
  $scope.photos = new Array();
  $http.get('js/embalses_photos.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      if($scope.item[1].Nombre_Reservorio == value.embalse){
        angular.forEach(value.photos, function(foto, k) {
          $http.get('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto).success(function(data){
            $scope.photos.push('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto);
          }).error(function(error){
          });
        });
      }
    });
  });

  $http.get('js/glosario.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      $scope.glosario.push(value);
    });
  });

  $scope.seePhotos = function(){
    $state.go('app.embalseSeleFotos', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelGene = function(){
    $state.go('app.embalseSeleInfoGene', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.embalseSeleRegisHis', {item: $scope.item});
  }

  $scope.showGlosario = function(){
    $state.go('app.embalseGlosario', {glosario: $scope.glosario});
  }
})

.controller('EmbalseSeleInfoGeneCtrl', function($scope, $state, $http) {
  $scope.photos = $state.params.photos;
  $scope.item = $state.params.item;
  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.embalseSeleRegisHis', {item: $scope.item});
  }
})

.controller('EmbalseSeleRegisHisCtrl', function($scope, $state, $http) {
  $scope.photos = $state.params.photos;
  $scope.item = $state.params.item;

  $scope.showChartByHis = function(){
    $state.go('app.embalseSeleRegisHisChart', {item: $scope.item});
  }
})

.controller('EmbalseSeleRegisHisChartCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.chartHisConfig = {
    chart: {
      type: 'column'
    },
    series: [{
      data: [10, 15, 12, 8, 7],
      id: 'series1'
    }],
    title: {
      text: 'Drinux'
    }
  }
})

.controller('EmbalseSeleFotosCtrl', function($scope, $state, $http) {
  $scope.photos = $state.params.photos;
  $scope.item = $state.params.item;
})

.controller('EmbalsesCtrl', ['$scope', '$ionicLoading', '$compile', '$http', 'EmbalsesDataService', 'PouchService', function($scope, $ionicLoading, $compile, $http, EmbalsesDataService, PouchService) {

    // http://stackoverflow.com/questions/33240837/angular-ionic-google-maps-on-state-change
    $scope.init = function() {

        var infowindow = null;

        var myLatlng = new google.maps.LatLng(-12.046374, -77.042793);

        var mapOptions = {
            center: myLatlng,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);

        infowindow = new google.maps.InfoWindow({
            content: "loading..."
        });

        $http.get('js/markers_data.json').then(function(response){
          for (i = 0; i < response.data.length; i++) {

              var sites = response.data[i];
              var siteLatLng = new google.maps.LatLng(sites.lat, sites.lon);

              // this is declared to pass as parameter {{sites}} to $compile
              $scope.sites = sites;

              var _temp = '<div>';
              _temp += '<div style="width: 100%;">';
              _temp += '<label style="display: inline-block;">Embalse:</label>';
              _temp += '<p style="display: inline-block; color: #666666;">&nbsp' +  sites.embalse + '</p>';
              _temp += '</div>';

              _temp += '<div style="width: 100%;">';
              _temp += '<label display: inline-block;>Autoridad Local del Agua:</label>';
              _temp += '<p style="display: inline-block; color: #666666;">&nbsp'+sites.ala+'</p>';
              _temp += '</div>';

              _temp += '<div style="width: 100%;">';
              _temp += '<label display: inline-block;>Región:</label>';
              _temp += '<p style="display: inline-block; color: #666666;">&nbsp'+sites.name+'</p>';
              _temp += '</div>';

              _temp += '<div style="width: 100%;">';
              _temp += '<label display: inline-block;>Volumen Total:</label>';
              _temp += '<p style="display: inline-block; color: #666666;">&nbsp'+sites.volumen_total+'</p>';
              _temp += '</div>';

              _temp += '<a ui-sref="app.embalse({itemEmbMap: {{sites}} })">Leer mas</a>';
              _temp += '</div>';

              var compiled = $compile(_temp)($scope);

              var marker = new google.maps.Marker({
                  position: siteLatLng,
                  map: map,
                  title: sites.name,
                  content: compiled[0]
              });

              google.maps.event.addListener(marker, "click", function () {
                  infowindow.setContent(this.content);
                  infowindow.open(map, this);
              });


              $scope.map = map;
          }
        });
    }

}])

.controller('EmbalseCtrl', function($scope, $state, $http) {
  $scope.mappage = 1;
  $scope.item = new Array();
  $scope.glosario = new Array();
  $scope.photos = new Array();
  $scope.item.push({name: $state.params.itemEmbMap.embalse});

  $http.get('js/embalses.json').then(function(response){
    $scope.misEmbalses;
    response.data.forEach(function (a) {
      if($state.params.itemEmbMap.embalse == a.Nombre_Reservorio){
        $scope.misEmbalse = a;
      }
    });
  });

  var _obj = {
    Nombre_Reservorio: $state.params.itemEmbMap.embalse,
    Capacid_Hidraul_Util: '20.55',
    Capacid_Hidraul_Alm: '36.688',
    Capacid_Hidraul_Alm_Porc: '86.4445'
  }

  $scope.item.push(_obj);

  /*Data Chart*/
  $scope.chartConfig = {
    chart: {
        type: 'bar',
        height: 150,
    },
    title: {
        text: ''
    },
    subtitle: {
        text: '(Volumen de Agua)'
    },
    xAxis: {
        categories: ['%']
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: ''
        }/*,
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }*/
    },
    legend: {
        reversed: true
    },
    tooltip: {
        pointFormat: 'Porcentaje: <b>{point.y:.1f} %</b>'
    },
    plotOptions: {
        bar: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
            }
        }
    },
    series: [{
        name: 'No Utilizada',
        data: [(100 - parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc))],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    },{
        name: 'Almacenada',
        data: [parseFloat($scope.item[1].Capacid_Hidraul_Alm_Porc)],
        pointWidth: 40,
        dataLabels: {
          formatter: function() {
            return  Highcharts.numberFormat(this.y, 2, '.');
          }
        }
    }]
  }
  /*Data Chart *******************************************************/

  $http.get('js/embalses_photos.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      if($scope.item[1].Nombre_Reservorio == value.embalse){
        angular.forEach(value.photos, function(foto, k) {
          $http.get('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto).success(function(data){
            $scope.photos.push('photos/' + $scope.item[1].Nombre_Reservorio + '/' + foto);
          }).error(function(error){

          });
        });
      }
    });
  });

  $http.get('js/glosario.json').then(function(response){
    angular.forEach(response.data, function(value, key) {
      $scope.glosario.push(value);
    });
  });

  $scope.seePhotos = function(){
    $state.go('app.embalseMapPhotos', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelGene = function(){
    $state.go('app.embalseMapSeleGene', {item: $scope.item, photos: $scope.photos});
  }

  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.embalseMapSeleRegisHis', {item: $scope.item});
  }

  $scope.showGlosario = function(){
    $state.go('app.embalseMapGlosario', {glosario: $scope.glosario});
  }
})

/*Shared Reports Pages Controllers*/
.controller('ReportesAAAPhotosCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.photos = $state.params.photos;
})

.controller('ReportesAAASeleGeneCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.photos = $state.params.photos;

  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.reportesAAASeleRegisHis', {item: $scope.item});
  }
})

.controller('ReportesAAASeleRegisHisCtrl', function($scope, $state, $http) {
  $scope.photos = $state.params.photos;
  $scope.item = $state.params.item;
  $scope.showChartByHis = function(){
    $state.go('app.reportesAAASeleRegisHisChart', {item: $scope.item});
  }
})

.controller('ReportesAAASeleRegisHisChartCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.chartHisConfig = {
    chart: {
      type: 'column'
    },
    series: [{
      data: [10, 15, 12, 8, 7],
      id: 'series1'
    }],
    title: {
      text: 'Drinux'
    }
  }
})

.controller('ReportesAAAGlosarioCtrl', function($scope, $state, $http, $ionicPopup) {
  $scope.glosario = $state.params.glosario;

  $scope.showDataGlosary = function(item){
    var alertPopup = $ionicPopup.alert({
      title: 'Significado de ' + item.palabra,
      subTitle: 'Resultado',
      template: item.significado,
      okText: 'Cerrar'
    });

    alertPopup.then(function(res) {

    });
  }
})

/*Shared Embalses Pages Controllers*/
.controller('EmbalseMapPhotosCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.photos = $state.params.photos;
})

.controller('EmbalseMapSeleGeneCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.photos = $state.params.photos;

  $scope.seeEmbalseSelRegisHis = function(){
    $state.go('app.embalseMapSeleRegisHis', {item: $scope.item});
  }
})

.controller('EmbalseMapSeleRegisHisCtrl', function($scope, $state, $http) {
  $scope.photos = $state.params.photos;
  $scope.item = $state.params.item;

  $scope.showChartByHis = function(){
    $state.go('app.embalseMapSeleRegisHisChart', {item: $scope.item});
  }
})

.controller('EmbalseMapSeleRegisHisChartCtrl', function($scope, $state, $http) {
  $scope.item = $state.params.item;
  $scope.chartHisConfig = {
    chart: {
      type: 'column'
    },
    series: [{
      data: [10, 15, 12, 8, 7],
      id: 'series1'
    }],
    title: {
      text: 'Drinux'
    }
  }
})

.controller('EmbalseMapGlosarioCtrl', function($scope, $state, $http, $ionicPopup) {
  $scope.glosario = $state.params.glosario;

  $scope.showDataGlosary = function(item){
    var alertPopup = $ionicPopup.alert({
      title: 'Significado de ' + item.palabra,
      subTitle: 'Resultado',
      template: item.significado,
      okText: 'Cerrar'
    });

    alertPopup.then(function(res) {

    });
  }
})

.controller('EmbalseGlosarioCtrl', function($scope, $state, $http, $ionicPopup) {
  $scope.glosario = $state.params.glosario;

  $scope.showDataGlosary = function(item){
    var alertPopup = $ionicPopup.alert({
      title: 'Significado de ' + item.palabra,
      subTitle: 'Resultado',
      template: item.significado,
      okText: 'Cerrar'
    });

    alertPopup.then(function(res) {

    });
  }
})

.controller('InfoCtrl', function($scope, $stateParams) {
})

.controller('AcercaCtrl', function($scope, $stateParams) {
})

;

